<?php
/**
 * @file
 * Plugin to provide access control/visibility based on length of
 * simplecontext argument (in URL).
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Viewmode Access"),
  'description' => t('Control access by type of viewmode_arg argument.'),
  'callback' => 'ctools_plugin_viewmodeaccess_ctools_access_check',
  'settings form' => 'ctools_plugin_viewmodeaccess_ctools_access_settings',
  'summary' => 'ctools_plugin_viewmodeaccess_ctools_access_summary',
  'required context' => new ctools_context_required(t('Viewmode context'), 'viewmodecontext'),
);

/**
 * Settings form for the 'by role' access plugin.
 */
function ctools_plugin_viewmodeaccess_ctools_access_settings(&$form, &$form_state, $conf) {
  dpm($form, &$form_state);
  $options = pm_viewmode_modes();
  $form['settings']['allowed_viewmodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select viewmodes that should be available'),
    '#options' => $options,
    '#default_value' => $conf['allowed_viewmodes'],
  );
}

/**
 * Check for access.
 */
function ctools_plugin_viewmodeaccess_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }
  $compare = ($context->arg_length > $conf['arg_length']);
  if (($compare && $conf['greater_than']) || (!$compare && !$conf['greater_than'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Provide a summary description based upon the checked roles.
 */
function ctools_plugin_viewmodeaccess_ctools_access_summary($conf, $context) {
  return t('Sample smmary');
}

