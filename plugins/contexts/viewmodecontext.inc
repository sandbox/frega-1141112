<?php
/**
 * @file
 * Sample ctools context type plugin that shows how to create a context from an arg.
 *
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("viewmodecontext"),
  'description' => t('A single "viewmodecontext" context, or data element.'),
  'context' => 'ctools_plugin_example_context_create_viewmodecontext',  // func to create context
  'context name' => 'viewmodecontext',
	'edit form' => 'viewmodecontext_settings_form',
  'settings form' => 'viewmodecontext_settings_form',
  'keyword' => 'viewmodecontext',

  // Provides a list of items which are exposed as keywords.
  'convert list' => 'viewmodecontext_convert_list',
  // Convert keywords into data.
  'convert' => 'viewmodecontext_convert',

  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter some data to represent this "viewmodecontext".'),
  ),
);

/**
 * Create a context, either from manual configuration or from an argument on the URL.
 *
 * @param $empty
 *   If true, just return an empty context.
 * @param $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param $conf
 *   TRUE if the $data is coming from admin configuration, FALSE if it's from a URL arg.
 *
 * @return
 *   a Context object/
 */
function ctools_plugin_example_context_create_viewmodecontext($empty, $data = NULL, $conf = FALSE) {
  // data is the argument
  $context = new ctools_context('viewmodecontext');
  $context->plugin = 'viewmodecontext';

  if ($empty) {
    return $context;
  }

  if ($conf) {
    if (!empty($data)) {
      $context->data = new stdClass();
      return $context;
    }
  }
  else {
    // $data is coming from an arg - it's just a string.
    // This is used for keyword.
    $context->title = $data;
    $context->argument = $data;
    // Validate argument
    
    return $context;
  }
}

function viewmodecontext_settings_form($conf, $external = FALSE) {
  if (empty($conf)) {
    $conf = array(
      'sample_viewmodecontext_setting' => 'default viewmodecontext setting',
    );
  }
  $form = array();
  $form['sample_viewmodecontext_setting'] = array(
    '#type' => 'textfield',
    '#title' => t('Setting for viewmodecontext'),
    '#size' => 50,
    '#description' => t('An example setting that could be used to configure a context'),
    '#default_value' => $conf['sample_viewmodecontext_setting'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  $form['sample_viewmodecontext_setting2'] = array(
    '#type' => 'textfield',
    '#title' => t('Setting for viewmodecontext'),
    '#size' => 50,
    '#description' => t('An example setting that could be used to configure a context'),
    '#default_value' => $conf['sample_viewmodecontext_setting'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  $form['sample_viewmodecontext_setting3'] = array(
    '#type' => 'textfield',
    '#title' => t('Setting for viewmodecontext'),
    '#size' => 50,
    '#description' => t('An example setting that could be used to configure a context'),
    '#default_value' => $conf['sample_viewmodecontext_setting'],
    '#prefix' => '<div class="clear-block no-float">',
    '#suffix' => '</div>',
  );
  return $form;
}



/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function viewmodecontext_convert_list() {
  return array();
}

/**
 * Convert a context into a string to be used as a keyword by content types, etc.
 */
function viewmodecontext_convert($context, $type) {
  return NULL;
}

