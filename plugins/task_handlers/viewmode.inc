<?php

/**
 * @file
 *
 * This is the task handler plugin to handle generating 403, 404 and 301 response codes.
 */

// Plugin definition
$plugin = array(
  // is a 'context' handler type, meaning it supports the API of the
  // context handlers provided by ctools context plugins.
  'handler type' => 'context',
  'visible' => TRUE, // may be added up front.

  // Administrative fields.
  'title' => t('View mode page'),
  'admin summary' => 'pm_viewmode_admin_summary',
  'admin title' => 'pm_viewmode_title',
  'operations' => array(
    'settings' => array(
      'title' => t('General'),
      'description' => t('Change settings for this variant.'),
      'form' => 'pm_viewmode_edit_settings',
    ),
    'criteria' => array(
      'title' => t('Selection rules'),
      'description' => t('Control the criteria used to decide whether or not this variant is used.'),
      'ajax' => FALSE,
      'form' => array(
        'order' => array(
          'form' => t('Selection rules'),
        ),
        'forms' => array(
          'form' => array(
            'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
            'form id' => 'ctools_context_handler_edit_criteria',
          ),
        ),
      ),
    ),
    'context' => array(
      'title' => t('Contexts'),
      'ajax' => FALSE,
      'description' => t('Add additional context objects to this variant that can be used by the content.'),
      'form' => array(
        'order' => array(
          'form' => t('Context'),
        ),
        'forms' => array(
          'form' => array(
            'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
            'form id' => 'ctools_context_handler_edit_context',
          ),
        ),
      ), 
    ),
  ),

  // Callback to render the data.
  'render' => 'pm_viewmode_render',

  'add features' => array(
    'criteria' => t('Selection rules'),
    // 'context' => t('Contexts'),
  ),
  // Where to go when finished.
  'add finish' => 'settings',

  'required forms' => array(
    'settings' => t('Panel settings'),
    'context' => t('Contexts')
  ),

  'edit forms' => array(
    'criteria' => t('Selection rules'),
    'settings' => t('General'),
    'context' => t('Contexts'),
  ),
  'forms' => array(
    'settings' => array(
      'form id' => 'pm_viewmode_edit_settings',
    ),
    'context' => array(
      'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
      'form id' => 'ctools_context_handler_edit_context',
    ),
    'criteria' => array(
      'include' => drupal_get_path('module', 'ctools') . '/includes/context-task-handler.inc',
      'form id' => 'ctools_context_handler_edit_criteria',
    ),
  ),
  'default conf' => array(
    'title' => t('Full view'),
    'contexts' => array(),
    'relationships' => array(),
    'argument' => '',
    'mode' => '',
    'title_options' => '',
    'custom_title' => '',
  ),
);



function pm_viewmode_argument_options($handler, $task, $subtask, $filter_by=false) {
  ctools_include('context');
  ctools_include('context-task-handler');
  // load the context by
  $object = ctools_context_handler_get_task_object($task, $subtask, $handler);
  $options = array();
  // 
  foreach ( (array) $object->arguments as $key => $info) {
    if (!$filter_by || strpos($info['name'], $filter_by)!==false ) {
      $options[$info['keyword']] = $info['identifier'];
    }
  }
  return $options;
}

function pm_viewmode_admin_summary($handler, $task, $subtask, $page, $show_title = TRUE) {
  $task_name = page_manager_make_task_name($task['name'], $subtask['name']);
  $output = '';

  ctools_include('context');
  ctools_include('context-task-handler');

  // Get the operations
  $operations = page_manager_get_operations($page);

  // Get operations for just this handler.
  $operations = $operations['handlers']['children'][$handler->name]['children']['actions']['children'];
  $args = array('handlers', $handler->name, 'actions');
  $rendered_operations = page_manager_render_operations($page, $operations, array(), array('class' => array('actions')), 'actions', $args);

  $plugin = page_manager_get_task_handler($handler->handler);

  $object = ctools_context_handler_get_task_object($task, $subtask, $handler);
  $display->context = ctools_context_load_contexts($object, TRUE);

  
  $access = ctools_access_group_summary(!empty($handler->conf['access']) ? $handler->conf['access'] : array(), $display->context);
  
  $modes = pm_viewmode_modes($handler, $task, $subtask);
  
  if ($access) {
    $access = t('Display View mode %mode will be used if @conditions.', array('@conditions' => $access, '%mode'=>$modes[$handler->conf['mode']]));
  }
  else {
    $access = t('Display build %mode will always be selected.', array('%mode'=>$modes[$handler->conf['mode']]));
  }

  $rows = array();

  $type = $handler->type == t('Default') ? t('In code') : $handler->type;
  $rows[] = array(
    array('class' => array('page-summary-label'), 'data' => t('Storage')),
    array('class' => array('page-summary-data'), 'data' => $type),
    array('class' => array('page-summary-operation'), 'data' => ''),
  );

  if (!empty($handler->disabled)) {
    $link = l(t('Enable'), page_manager_edit_url($task_name, array('handlers', $handler->name, 'actions', 'enable')));
    $text = t('Disabled');
  }
  else {
    $link = l(t('Disable'), page_manager_edit_url($task_name, array('handlers', $handler->name, 'actions', 'disable')));
    $text = t('Enabled');
  }

  $rows[] = array(
    array('class' => array('page-summary-label'), 'data' => t('Status')),
    array('class' => array('page-summary-data'), 'data' => $text),
    array('class' => array('page-summary-operation'), 'data' => $link),
  );

  $link = l(t('Edit'), page_manager_edit_url($task_name, array('handlers', $handler->name, 'criteria')));
  $rows[] = array(
    array('class' => array('page-summary-label'), 'data' => t('Selection rule')),
    array('class' => array('page-summary-data'), 'data' => $access),
    array('class' => array('page-summary-operation'), 'data' => $link),
  );

  $link = l(t('Edit'), page_manager_edit_url($task_name, array('handlers', $handler->name, 'settings')));
  
  $arguments = pm_viewmode_argument_options($handler, $task, $subtask);
  $rows[] = array(
    array('class' => array('page-summary-label'), 'data' => t('Context object to render')),
    array('class' => array('page-summary-data'), 'data' => $arguments[$handler->conf['argument']]),
    array('class' => array('page-summary-operation'), 'data' => $link),
  );
  
  $rows[] = array(
    array('class' => array('page-summary-label'), 'data' => t('View mode')),
    array('class' => array('page-summary-data'), 'data' => $modes[$handler->conf['mode']]),
    array('class' => array('page-summary-operation'), 'data' => $link),
  );
  
  switch ($handler->conf['title_options']) {
    case 'hide': $label = t('Hidden'); break;
    case 'show': $label = t('Show argument-object title'); break;
    case 'custom': $label = t('Custom title: @custom_title', array('@custom_title'=>$handler->conf['custom_title'])); break;
  } 
  
  $rows[] = array(
    array('class' => array('page-summary-label'), 'data' => t('Title')),
    array('class' => array('page-summary-data'), 'data' => $label),
    array('class' => array('page-summary-operation'), 'data' => $link),
  );

  $info = theme('table', array('header' => array(), 'rows' => $rows, 'attributes' => array('class' => array('page-manager-handler-summary'))));

  $title = $handler->conf['title'];
  if ($title != t('View mode page')) {
    $title = t('View mode page: @title', array('@title' => $title));
  }

  $output .= '<div class="clearfix">';
  if ($show_title) {
  $output .= '<div class="handler-title clearfix">';
    $output .= '<div class="actions handler-actions">' . $rendered_operations['actions'] . '</div>';
    $output .= '<span class="title-label">' . $title . '</span>';
  }

  $output .= '</div>';
  $output .= $info;
  $output .= '</div>';

  return $output;
}

/**
 * Set up a title for the panel based upon the selection rules.
 */
function pm_viewmode_title($handler, $task, $subtask) {
  if (isset($handler->conf['title'])) {
    return check_plain($handler->conf['title']);
  }
  else {
    return t('View mode page');
  }
}

/**
 * General settings for the panel
 */
function pm_viewmode_edit_settings(&$form, &$form_state) {
  $conf = $form_state['handler']->conf;
  $form['title'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['title'],
    '#title' => t('Administrative title'),
    '#description' => t('Administrative title of this variant.'),
  );
  
  $options = pm_viewmode_argument_options($form_state['handler'], $form_state['task'], $form_state['subtask'], 'entity_id');

  $form['argument'] = array(
    '#title' => t('Context entity to render'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['argument'],
  );
  
  // @todo: make this dependent!
  $modes = pm_viewmode_modes($form_state['handler'], $form_state['task'], $form_state['subtask']);
  
 
  
  $form['mode'] = array(
    '#title' => t('View mode'),
    '#type' => 'select',
    '#options' => $modes, 
    '#default_value' => $conf['mode'],
  );
  
  
  
  
  $form['title_options'] = array(
    '#title' => t('Title options'),
    '#type' => 'select',
    '#options' => array('show'=>t('Show title'), 'hide'=>t('Hide title'), 'custom'=>t('Custom title')),
    '#default_value' => $conf['title_options'],
  );

  ctools_include('dependent');
  $form['custom_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom title'),
    '#default_value' => $conf['custom_title'],
    '#process' => array('ctools_dependent_process'),
    '#dependency' => array('edit-title-options' => array('custom')),
    '#description' => t('Enter the custom title. You may use keyword substitutions from contexts.'),
  );  
  
  return $form;
}


function pm_viewmode_edit_settings_submit(&$form, &$form_state) {
  $form_state['handler']->conf['argument'] = $form_state['values']['argument'];
  $form_state['handler']->conf['mode'] = $form_state['values']['mode'];
  $form_state['handler']->conf['title'] = $form_state['values']['title'];
  $form_state['handler']->conf['title_options'] = $form_state['values']['title_options'];
  $form_state['handler']->conf['custom_title'] = $form_state['values']['custom_title'];
}

function pm_viewmode_render($handler, $base_contexts, $args, $test = TRUE) {
  ctools_include('context');
  ctools_include('context-task-handler');

  // Add my contexts
  $contexts = ctools_context_handler_get_handler_contexts($base_contexts, $handler);

  // Test.
  if ($test && !ctools_context_handler_select($handler, $contexts)) {
    return;
  }

  if (isset($handler->handler)) {
    ctools_context_handler_pre_render($handler, $contexts, $args);
  }
  
  // find the right context
  $type = false; 
  foreach ($contexts as $context ) {
    if ($context->keyword == $handler->conf['argument']) {
      // oh, why sometimes an array, and sometimes a string ... meh ... 
      if (is_array($context->type)) {
        $type = $context->type[2];
      }  else {
        $type = $context->type;
      }
      $object = $context; 
    } 
  }
  
  // configure the title of the page
  switch ($handler->conf['title_options']) {
    case 'hide': $title = false; break;
    case 'custom':
      $title = ctools_context_keyword_substitute($handler->conf['custom_title'], array(), $contexts);
      break;
    case 'show':
    default:
      // to handle entity types? use entity.module ...
      switch ($type) {
        case 'user': $title = $object->data->name; break;
        case 'node': $title = $object->data->title; break;  
      }
      
  }
  
  // setup the title 
  $info = array('title' => $title);

  // split and determine
  list($viewmode_type_or_entity_type, $mode) = explode(":", $handler->conf['mode']);
  // see if we take the viewmode from the context/argument
  if ($viewmode_type_or_entity_type=='viewmode_arg') {
    //
    foreach ($contexts as $id => $context) {
      if ($context->keyword == $mode) {
        $mode = check_plain($context->argument);
      }
    }
  }
  
  
  // @todo use entity.modules - entity_view?
  switch ($type) {
    case 'node': $info['content'] = node_view($object->data, $mode); break;
    case 'user': $info['content'] = user_view($object->data, $mode); break;
    // @todo: comment etc.
    default:
      $info['content'] = t('Invalid type @type', array('@type'=>$type));
      break; 
  }
  return $info;
}